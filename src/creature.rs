use amethyst::ecs::prelude::*;
use amethyst::core::transform::Transform;
use amethyst::animation::{AnimationSet, AnimationControlSet, get_animation_set, EndControl, AnimationCommand};

#[derive(Debug, Clone, Copy, PartialEq, Component)]
pub struct Creature {
    pub entity: Entity,
    pub health: i32,
    pub iframes: f32,
    pub iframes_const: f32,
    pub animation: Option<usize>,
    pub run_animation: usize,
}

impl Creature {
    pub fn hurt(&mut self) -> bool {
        self.iframes = self.iframes_const;
        self.health -= 1;
        self.health <= 0
    }

    pub fn run_animation(
        &mut self,
        sets: &ReadStorage<AnimationSet<usize, Transform>>,
        controls: &mut WriteStorage<AnimationControlSet<usize, Transform>>,
    ) {
        if self.animation.is_some() {
            return;
        }

        if let Some(animations) = sets.get(self.entity) {
            let animation = animations.animations.get(&self.run_animation).unwrap();
            let mut set = get_animation_set::<usize, Transform>(controls, self.entity).unwrap();
            set.add_animation(
                self.run_animation,
                animation,
                EndControl::Loop(None),
                1.0,
                AnimationCommand::Start,
            );
            self.animation = Some(self.run_animation);
        }
    }
}
