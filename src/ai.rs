use amethyst::ecs::prelude::*;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Component)]
pub struct Target {
    pub entity: Entity,
}
