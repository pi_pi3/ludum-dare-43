use amethyst::input::InputHandler;
use amethyst::core::timing::Time;
use amethyst::ecs::{Join, System, Read, ReadStorage, WriteStorage};

use crate::game::Consts;
use crate::player::{Player, Zmobie};
use crate::physics::{Vec3, Rotation, Acceleration};

/// Simple player controller.
pub struct ControllerSystem;

impl<'s> System<'s> for ControllerSystem {
    type SystemData = (
        WriteStorage<'s, Rotation>,
        WriteStorage<'s, Acceleration>,
        ReadStorage<'s, Player>,
        ReadStorage<'s, Zmobie>,
        Read<'s, InputHandler<String, String>>,
        Read<'s, Time>,
        Read<'s, Consts>,
    );

    fn run(&mut self, (mut rotations, mut accels, players, zmobies, input, time, consts): Self::SystemData) {
        let dt = time.delta_seconds();

        let vertical = input.axis_value("vertical").unwrap_or_default() as f32;
        let horizontal = input.axis_value("horizontal").unwrap_or_default() as f32;
        let rotation = input.axis_value("rotation").unwrap_or_default() as f32;

        for (rot, accel, _, _) in (&mut rotations, &mut accels, &players, !&zmobies).join() {
            rot.0 += (consts.rotation_speed * rotation).to_radians() * dt;
            let v = rot.0.cos() * vertical - rot.0.sin() * horizontal;
            let h = rot.0.sin() * vertical + rot.0.cos() * horizontal;
            accel.0 = Vec3::new(consts.max_accel * h, 0.0, consts.max_accel * v);
        }
    }
}
