use amethyst::core::timing::Time;
use amethyst::core::transform::{Transform, Parent};
use amethyst::ecs::{Entity, Join, System, Read, ReadStorage, WriteStorage, Entities, LazyUpdate};
use amethyst::renderer::Camera;
use amethyst::assets::Handle;
use amethyst::utils::tag::Tag;
use amethyst_gltf::GltfSceneAsset;

use game::Consts;
use player::{PlayerMarker, Player, Zmobie};
use spike::Spike;

use physics::{Rotation, Position, Velocity, Acceleration, Friction, Collider};
use physics::intersection::Intersection;

/// Cartoonish physics simulation.
pub struct PhysicsSystem;

impl<'s> System<'s> for PhysicsSystem {
    type SystemData = (
        WriteStorage<'s, Transform>,
        ReadStorage<'s, Rotation>,
        WriteStorage<'s, Position>,
        WriteStorage<'s, Velocity>,
        WriteStorage<'s, Acceleration>,
        ReadStorage<'s, Friction>,
        WriteStorage<'s, Collider>,
        ReadStorage<'s, Player>,
        WriteStorage<'s, Zmobie>,
        ReadStorage<'s, Spike>,
        WriteStorage<'s, Parent>,
        ReadStorage<'s, Camera>,
        ReadStorage<'s, Handle<GltfSceneAsset>>,
        ReadStorage<'s, Tag<PlayerMarker>>,
        Entities<'s>,
        Read<'s, LazyUpdate>,
        Read<'s, Time>,
        Read<'s, Consts>,
    );

    fn run(
        &mut self,
        (
            mut transforms,
            rotations,
            mut poss,
            mut vels,
            mut accels,
            frictions,
            mut colliders,
            players,
            mut zmobies,
            spikes,
            mut parents,
            cameras,
            gltfs,
            player_markers,
            entities,
            lazy,
            time,
            consts,
        ): Self::SystemData,
    ) {
        let dt = time.delta_seconds();

        for accel in (&mut accels).join() {
            accel.0.x = accel.0.x.abs().min(consts.max_accel) * accel.0.x.signum();
            accel.0.y = accel.0.y.abs().min(consts.max_accel) * accel.0.y.signum();
            accel.0.z = accel.0.z.abs().min(consts.max_accel) * accel.0.z.signum();
        }

        for (vel, accel) in (&mut vels, &accels).join() {
            vel.0 += accel.0 * dt;
        }

        for vel in (&mut vels).join() {
            vel.0.x = vel.0.x.abs().min(consts.max_vel) * vel.0.x.signum();
            vel.0.y = vel.0.y.abs().min(consts.max_vel) * vel.0.y.signum();
            vel.0.z = vel.0.z.abs().min(consts.max_vel) * vel.0.z.signum();
        }

        for (pos, vel, _) in (&mut poss, &vels, !&colliders).join() {
            pos.0 += vel.0 * dt;
        }

        fn update_pos<'s>(
            e_a: Entity,
            pos: &mut Position,
            vel: &Velocity,
            collider: &Collider,
            entities: &Entities<'s>,
            colliders: &WriteStorage<'s, Collider>,
            dt: f32,
        ) -> Vec<Entity> {
            let mut collisions = vec![];

            pos.0 += vel.0 * dt;
            let mut collider_a: Collider = collider.clone();
            collider_a.update_position(pos.0);
            for (e_b, collider_b) in (entities, colliders).join() {
                if e_a != e_b {
                    if let Some(Intersection { offset, .. }) = collider_a.intersects(collider_b) {
                        collisions.push(e_b);
                        if collider_a.movable {
                            pos.0 += offset;
                        }
                    } else if let Some(Intersection { offset, .. }) = collider_b.intersects(&collider_a) {
                        collisions.push(e_b);
                        if collider_a.movable {
                            pos.0 -= offset;
                        }
                    }
                }
            }

            collisions
        }

        for (e_a, mut pos, vel, collider_a, _) in (&entities, &mut poss, &vels, &colliders, !&players).join() {
            update_pos(e_a, pos, vel, collider_a, &entities, &colliders, dt);
        }

        for (e_a, mut pos, vel, collider_a, _) in (&entities, &mut poss, &vels, &colliders, &zmobies).join() {
            update_pos(e_a, pos, vel, collider_a, &entities, &colliders, dt);
        }

        let mut zmobiefy = None;
        for (e_a, mut pos, vel, collider_a, _, _) in
            (&entities, &mut poss, &vels, &colliders, &players, !&zmobies).join()
        {
            let collisions = update_pos(e_a, pos, vel, collider_a, &entities, &colliders, dt);
            for e_b in collisions {
                if spikes.get(e_b).is_some() {
                    zmobiefy = Some(e_a);
                }
            }
        }
        if let Some(e) = zmobiefy {
            Player::zmobiefy(e, &mut zmobies);

            let gltf = (&gltfs, &player_markers)
                .join()
                .map(|(gltf, _)| gltf.clone())
                .next()
                .expect("no player mesh loaded");

            let builder = lazy.create_entity(&entities);
            let e = builder.entity;
            Player::with_lazy(builder, gltf, &consts);
            for (_, parent) in (&cameras, &mut parents).join() {
                parent.entity = e;
            }
        }

        for (pos, mut collider) in (&poss, &mut colliders).join() {
            collider.update_position(pos.0);
        }

        for (pos, transform) in (&poss, &mut transforms).join() {
            transform.set_xyz(pos.0.x, pos.0.y, pos.0.z);
        }

        for (rot, transform) in (&rotations, &mut transforms).join() {
            transform.set_rotation_euler(0.0, rot.0, 0.0);
        }

        for (vel, friction) in (&mut vels, &frictions).join() {
            let friction = (friction.0 - 1.0) * dt + 1.0;
            vel.0 *= friction.recip();
        }

        for (accel, friction) in (&mut accels, &frictions).join() {
            let friction = (friction.0 - 1.0) * dt + 1.0;
            accel.0 *= friction.recip();
        }
    }
}
