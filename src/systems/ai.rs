use amethyst::core::timing::Time;
use amethyst::core::transform::Transform;
use amethyst::ecs::{Join, System, Read, Write, ReadStorage, WriteStorage, Entities, LazyUpdate};
use amethyst::animation::{AnimationSet, AnimationControlSet};

use game::{Consts, Meshes};
use creature::Creature;
use player::{Player, Zmobie};
use goblin::{Goblin, Wavesize};
use ai::Target;

use physics::{Rotation, Position, Acceleration};

/// AI simulation.
pub struct AiSystem;

impl<'s> System<'s> for AiSystem {
    type SystemData = (
        ReadStorage<'s, Target>,
        ReadStorage<'s, Position>,
        WriteStorage<'s, Rotation>,
        WriteStorage<'s, Acceleration>,
        WriteStorage<'s, Creature>,
        ReadStorage<'s, Player>,
        ReadStorage<'s, Zmobie>,
        ReadStorage<'s, Goblin>,
        ReadStorage<'s, AnimationSet<usize, Transform>>,
        WriteStorage<'s, AnimationControlSet<usize, Transform>>,
        Entities<'s>,
        Read<'s, Meshes>,
        Read<'s, LazyUpdate>,
        Read<'s, Time>,
        Read<'s, Consts>,
        Write<'s, Wavesize>,
    );

    fn run(
        &mut self,
        (
            targets,
            poss,
            mut rotations,
            mut accels,
            mut creatures,
            players,
            zmobies,
            goblins,
            sets,
            mut control,
            entities,
            meshes,
            lazy,
            time,
            consts,
            mut wavesize,
        ): Self::SystemData,
    ) {
        let dt = time.delta_seconds();

        for (entity, target, pos_a, rot, accel) in (&entities, &targets, &poss, &mut rotations, &mut accels).join() {
            if let Some(pos_b) = (&poss).join().get(target.entity, &entities) {
                let d = pos_b.0 - pos_a.0;
                let d_mag = (d.x * d.x + d.y * d.y + d.z * d.z).sqrt();
                let d_norm = d * d_mag.recip();
                rot.0 = d_norm.x.atan2(d_norm.z) + std::f32::consts::PI;
                *accel = Acceleration(d_norm * consts.max_accel);

                if d_mag <= consts.attack_dist {
                    if let Some((goblin, _)) = (&mut creatures, &goblins).join().get(target.entity, &entities) {
                        if goblin.iframes <= 0. {
                            let dead = goblin.hurt();
                            if dead {
                                lazy.remove::<Target>(entity);
                                entities.delete(goblin.entity).unwrap();
                            }
                        }
                    }
                    if let Some((player, _)) = (&mut creatures, &players).join().get(target.entity, &entities) {
                        if player.iframes <= 0. {
                            let dead = player.hurt();
                            if dead {
                                lazy.remove::<Target>(entity);
                                entities.delete(player.entity).unwrap();
                            }
                        }
                    }
                }
            }
        }

        for (_, goblin, target) in (&goblins, &creatures, &targets).join() {
            if !entities.is_alive(target.entity) {
                lazy.remove::<Target>(goblin.entity);
            }
        }

        for (_, goblin, _) in (&goblins, &creatures, !&targets).join() {
            if let Some((_, player, _)) = (&players, &creatures, !&zmobies).join().next() {
                lazy.insert(goblin.entity, Target { entity: player.entity });
            }
        }

        for (zmobie, _, _, target) in (&creatures, &players, &zmobies, &targets).join() {
            if !entities.is_alive(target.entity) {
                lazy.remove::<Target>(zmobie.entity);
            }
        }

        for (zmobie, _, pos_a, _, _) in (&creatures, &players, &poss, &zmobies, !&targets).join() {
            let mut dist = std::f32::INFINITY;
            let mut target = None;
            for (_, goblin, pos_b) in (&goblins, &creatures, &poss).join() {
                let d = pos_b.0 - pos_a.0;
                let d_mag = (d.x * d.x + d.y * d.y + d.z * d.z).sqrt();
                if d_mag < dist {
                    dist = d_mag;
                    target = Some(goblin.entity);
                }
            }
            if let Some(target) = target {
                lazy.insert(zmobie.entity, Target { entity: target });
            }
        }

        for creature in (&mut creatures).join() {
            creature.iframes -= dt;
            creature.run_animation(&sets, &mut control);
        }

        let goblin_count = (&goblins).join().fold(0, |acc, _| acc + 1);
        if goblin_count == 0 {
            if let Some(gltf) = meshes.goblin_handle.as_ref() {
                let size = wavesize.next();
                Goblin::wave(size, &lazy, &entities, gltf, &consts);
            }
        }
    }
}
