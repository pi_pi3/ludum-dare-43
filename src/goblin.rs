use rand::random;

use amethyst::prelude::*;
use amethyst::ecs::prelude::*;
use amethyst::ecs::world::{LazyBuilder, LazyUpdate};
use amethyst::core::transform::Transform;
use amethyst::assets::Handle;
use amethyst_gltf::GltfSceneAsset;

use game::Consts;
use creature::Creature;
use physics::{Vec3, Rotation, Position, Velocity, Acceleration, Friction, Collider, Shape, Sphere};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Wavesize(usize);

impl Wavesize {
    pub fn next(&mut self) -> usize {
        let size = self.0;
        self.0 = (self.0 as f32 * 1.4) as usize;
        size
    }
}

impl Default for Wavesize {
    fn default() -> Wavesize {
        Wavesize(5)
    }
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Deserialize, Serialize, Component)]
#[storage(NullStorage)]
pub struct GoblinMarker;

#[derive(Debug, Default, Clone, Copy, PartialEq, Component)]
#[storage(NullStorage)]
pub struct Goblin;

impl Goblin {
    pub fn with_lazy<'a>(builder: LazyBuilder<'a>, gltf: Handle<GltfSceneAsset>, consts: &Consts) {
        let e = builder.entity;
        let x = random::<f32>() * 2. - 1.;
        let z = random::<f32>() * 2. - 1.;
        let x = x.abs().max(0.5) * x.signum() * 64.;
        let z = z.abs().max(0.5) * z.signum() * 64.;
        let pos = Vec3::new(x, 0., z);
        builder
            .with(Goblin)
            .with(Creature {
                entity: e,
                health: consts.goblin_health,
                iframes: 0.,
                iframes_const: consts.goblin_iframes,
                animation: None,
                run_animation: consts.goblin_run_animation,
            })
            .with(gltf)
            .with(Transform::default())
            .with(Rotation::default())
            .with(Position(pos))
            .with(Velocity::default())
            .with(Acceleration::default())
            .with(Friction(consts.goblin_friction))
            .with(Collider::new(true, Shape::Sphere(Sphere::with_position(0.25, pos))))
            .build();
    }

    pub fn wave(size: usize, lazy: &LazyUpdate, entities: &Entities, gltf: &Handle<GltfSceneAsset>, consts: &Consts) {
        for _ in 0..size {
            Goblin::with_lazy(lazy.create_entity(entities), gltf.clone(), consts);
        }
    }
}
