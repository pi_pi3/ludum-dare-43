use amethyst::{Error, Result};
use amethyst::prelude::*;
use amethyst::renderer::*;
use amethyst::core::{SystemBundle, ArcThreadPool};
use amethyst::ecs::{Entity, System, Dispatcher, DispatcherBuilder};
use amethyst::ecs::prelude::*;
use amethyst::input::is_close_requested;
use amethyst::assets::{
    AssetPrefab, Completion, Handle, Prefab, PrefabData, PrefabError, PrefabLoader,
    ProgressCounter, RonFormat,
};
use amethyst::core::transform::{Transform, Parent};
use amethyst::ecs::prelude::Write;
use amethyst::utils::tag::{Tag, TagFinder};
use amethyst::utils::application_root_dir;
use amethyst_gltf::{GltfSceneAsset, GltfSceneFormat};

use serde::Deserialize;

use player::{PlayerMarker, Player};
use spike::{SpikesMarker, Spike};
use goblin::{GoblinMarker, Wavesize};

#[derive(Deserialize, Serialize)]
pub struct Consts {
    pub camera_xrot: f32,
    pub camera_ypos: f32,
    pub camera_zpos: f32,
    pub player_run_animation: usize,
    pub player_iframes: f32,
    pub player_friction: f32,
    pub player_health: i32,
    pub goblin_run_animation: usize,
    pub goblin_iframes: f32,
    pub goblin_friction: f32,
    pub goblin_health: i32,
    pub max_vel: f32,
    pub max_accel: f32,
    pub rotation_speed: f32,
    pub attack_dist: f32,
}

impl Consts {
    pub fn try_load() -> Option<Consts> {
        let file = format!("{}/resources/consts.ron", application_root_dir());
        let string = std::fs::read_to_string(&file).ok()?;
        let mut de = ron::de::Deserializer::from_str(&string).ok()?;
        Consts::deserialize(&mut de).ok()
    }
}

impl Default for Consts {
    fn default() -> Consts {
        Consts {
            camera_xrot: -60.,
            camera_ypos: 15.,
            camera_zpos: 7.,
            player_run_animation: 0,
            player_iframes: 1.,
            player_friction: 10.,
            player_health: 3,
            goblin_run_animation: 0,
            goblin_iframes: 1.5,
            goblin_friction: 10.,
            goblin_health: 5,
            max_vel: 40.,
            max_accel: 20.,
            rotation_speed: 90.,
            attack_dist: 1.,
        }
    }
}

#[derive(Default)]
pub struct Scene {
    scene_handle: Option<Handle<Prefab<ScenePrefabData>>>,
}

#[derive(Default)]
pub struct Meshes {
    pub player_handle: Option<Handle<GltfSceneAsset>>,
    pub goblin_handle: Option<Handle<GltfSceneAsset>>,
    pub spikes_handle: Option<Handle<GltfSceneAsset>>,
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Deserialize, Serialize, Component)]
#[storage(NullStorage)]
pub struct CameraMarker;

#[derive(Default, Deserialize, Serialize, PrefabData)]
#[serde(default)]
pub struct ScenePrefabData {
    transform: Option<Transform>,
    gltf: Option<AssetPrefab<GltfSceneAsset, GltfSceneFormat>>,
    camera: Option<CameraPrefab>,
    light: Option<LightPrefab>,
    player_tag: Option<Tag<PlayerMarker>>,
    camera_tag: Option<Tag<CameraMarker>>,
    goblin_mesh_tag: Option<Tag<GoblinMarker>>,
    spikes_mesh_tag: Option<Tag<SpikesMarker>>,
}

/// State enum
#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub enum GameState {
    Gameplay,
}

/// Game data contains the `Dispatcher`s and other data the world might need
pub struct GameData<'a, 'b> {
    core: Dispatcher<'a, 'b>,
    gameplay: Dispatcher<'a, 'b>,
}

impl<'a, 'b> GameData<'a, 'b> {
    /// Dispatch systems
    ///
    /// # Parameters
    ///
    /// `world: &World` The world in which the resources will be dispatched.  
    /// `state: GameState` The current game state.
    pub fn update(&mut self, world: &World, state: GameState) {
        self.core.dispatch(&world.res);
        match state {
            GameState::Gameplay => self.gameplay.dispatch(&world.res),
        }
    }
}

/// Builds a `GameData` struct with various bundles and systems.
pub struct GameDataBuilder<'a, 'b> {
    pub core: DispatcherBuilder<'a, 'b>,
    pub gameplay: DispatcherBuilder<'a, 'b>,
}

impl<'a, 'b> GameDataBuilder<'a, 'b> {
    /// Create a new builder.
    pub fn new() -> GameDataBuilder<'a, 'b> {
        GameDataBuilder {
            core: DispatcherBuilder::new(),
            gameplay: DispatcherBuilder::new(),
        }
    }

    /// Add a bundle which will be present at all times
    ///
    /// # Parameters
    ///
    /// `bundle: B` The bundle that will be built with the core dispatcher.
    pub fn with_core_bundle<B: SystemBundle<'a, 'b>>(mut self, bundle: B) -> Result<Self> {
        bundle.build(&mut self.core).map_err(Error::Core)?;
        Ok(self)
    }

    // /// Add a bundle which will be present only in one state
    // ///
    // /// # Parameters
    // ///
    // /// `state: GameState` The state to which the bundle will be bound.
    // /// `bundle: B` The bundle that will be built with the state dispatcher.
    // pub fn with_bundle<B: SystemBundle<'a, 'b>>(mut self, state: GameState, bundle: B) -> Result<Self> {
    //     match state {
    //         GameState::Gameplay => {
    //             bundle.build(&mut self.gameplay).map_err(Error::Core)?;
    //         }
    //     }
    //     Ok(self)
    // }

    /// Add a system which will be present at all times
    ///
    /// # Parameters
    ///
    /// `system: S` The system that will be built with the core dispatcher.
    /// `name: &str` Name of the system.
    /// `dependencies: &[&str]` Systems that this system depends on.
    pub fn with_core<S>(mut self, system: S, name: &str, dependencies: &[&str]) -> Self
    where
        S: for<'c> System<'c> + Send + 'a,
    {
        self.core.add(system, name, dependencies);
        self
    }

    /// Add a system which will be present at all times
    ///
    /// # Parameters
    ///
    /// `state: GameState` The state to which the state will be bound.
    /// `system: S` The system that will be built with the state dispatcher.
    /// `name: &str` Name of the system.
    /// `dependencies: &[&str]` Systems that this system depends on.
    pub fn with<S>(mut self, state: GameState, system: S, name: &str, dependencies: &[&str]) -> Self
    where
        S: for<'c> System<'c> + Send + 'a,
    {
        match state {
            GameState::Gameplay => self.gameplay.add(system, name, dependencies),
        }
        self
    }
}

impl<'a, 'b> DataInit<GameData<'a, 'b>> for GameDataBuilder<'a, 'b> {
    /// Build the `GameData`
    ///
    /// # Parameters
    ///
    /// `world: &mut World` The world in which the `Dispatcher`s will be setup.
    fn build(self, world: &mut World) -> GameData<'a, 'b> {
        let pool = world.read_resource::<ArcThreadPool>().clone();

        let mut core = self.core.with_pool(pool.clone()).build();
        let mut gameplay = self.gameplay.with_pool(pool.clone()).build();
        core.setup(&mut world.res);
        gameplay.setup(&mut world.res);

        GameData { core, gameplay }
    }
}

/// Gameplay State
#[derive(Default)]
pub struct Gameplay {
    progress: Option<ProgressCounter>,
}

impl<'a, 'b> State<GameData<'a, 'b>, StateEvent> for Gameplay {
    fn on_start(&mut self, data: StateData<GameData>) {
        let world = data.world;

        world.add_resource(Consts::try_load().unwrap_or_default());
        world.add_resource(Wavesize::default());
        world.add_resource(Meshes::default());
        self.progress = Some(ProgressCounter::default());
        {
            let mut skybox = world.write_resource::<SkyboxColor>();
            skybox.zenith = Rgba(0.0, 0.4, 0.6, 1.0);
            skybox.nadir = Rgba(0.0, 0.0, 0.0, 1.0);
        }

        world.exec(|(loader, mut scene): (PrefabLoader<ScenePrefabData>, Write<Scene>)| {
            scene.scene_handle = Some(loader.load("prefab/scene.ron", RonFormat, (), self.progress.as_mut().unwrap()));
        });
    }

    fn update(&mut self, data: StateData<GameData>) -> Trans<GameData<'a, 'b>, StateEvent> {
        let just_done = match self.progress.as_ref().map(|p| p.complete()) {
            Some(Completion::Loading) => false,

            Some(Completion::Complete) => {
                let scene_handle = data
                    .world
                    .read_resource::<Scene>()
                    .scene_handle
                    .as_ref()
                    .unwrap()
                    .clone();
                data.world.create_entity().with(scene_handle).build();
                true
            }

            Some(Completion::Failed) => {
                println!("Error: {:?}", self.progress.as_ref().unwrap().errors());
                return Trans::Quit;
            }

            None => false,
        };

        data.data.update(&data.world, GameState::Gameplay);
        data.world.maintain();

        if just_done {
            self.progress = None;
            if let Some(player) = data.world.exec(|finder: TagFinder<PlayerMarker>| finder.find()) {
                let consts = data.world.read_resource::<Consts>();
                let mut meshes = data.world.write_resource::<Meshes>();
                let gltf = data
                    .world
                    .read_storage::<Handle<GltfSceneAsset>>()
                    .get(player)
                    .map(|gltf| gltf.clone())
                    .unwrap();
                meshes.player_handle = Some(gltf.clone());

                let entities = data.world.entities();

                let lazy = data.world.read_resource::<LazyUpdate>();
                let builder = lazy.create_entity(&entities);
                let e = builder.entity;
                Player::with_lazy(builder, gltf, &consts);

                let cameras = data.world.read_storage::<Camera>();
                let mut parents = data.world.write_storage::<Parent>();
                for (_, parent) in (&cameras, &mut parents).join() {
                    parent.entity = e;
                }

                if let Some(ref mut transform) = data.world.write_storage::<Transform>().get_mut(player) {
                    transform.set_xyz(0., -5., 0.);
                }
            }

            if let Some(camera) = data.world.exec(|finder: TagFinder<CameraMarker>| finder.find()) {
                let consts = data.world.read_resource::<Consts>();
                data.world
                    .write_storage::<Transform>()
                    .insert(camera, {
                        let mut transform = Transform::default();
                        transform.set_rotation_euler(consts.camera_xrot.to_radians(), 0.0, 0.0);
                        transform.set_xyz(0.0, consts.camera_ypos, consts.camera_zpos);
                        transform
                    })
                    .unwrap();
            }

            if let Some(spike) = data.world.exec(|finder: TagFinder<SpikesMarker>| finder.find()) {
                let mut meshes = data.world.write_resource::<Meshes>();
                let gltf = data
                    .world
                    .read_storage::<Handle<GltfSceneAsset>>()
                    .get(spike)
                    .map(|gltf| gltf.clone())
                    .unwrap();
                meshes.spikes_handle = Some(gltf.clone());

                let entities = data.world.entities();

                let lazy = data.world.read_resource::<LazyUpdate>();

                for _ in 0..20 {
                    let builder = lazy.create_entity(&entities);
                    Spike::with_lazy(builder, gltf.clone());
                }

                if let Some(ref mut transform) = data.world.write_storage::<Transform>().get_mut(spike) {
                    transform.set_xyz(0., -5., 0.);
                }
            }
            if let Some(goblin) = data.world.exec(|finder: TagFinder<GoblinMarker>| finder.find()) {
                let mut meshes = data.world.write_resource::<Meshes>();
                let gltf = data
                    .world
                    .read_storage::<Handle<GltfSceneAsset>>()
                    .get(goblin)
                    .map(|gltf| gltf.clone())
                    .unwrap();
                meshes.goblin_handle = Some(gltf.clone());

                if let Some(ref mut transform) = data.world.write_storage::<Transform>().get_mut(goblin) {
                    transform.set_xyz(0., -5., 0.);
                }
            }
        }

        Trans::None
    }

    fn handle_event(&mut self, _data: StateData<GameData>, event: StateEvent) -> Trans<GameData<'a, 'b>, StateEvent> {
        match event {
            StateEvent::Window(ref event) if is_close_requested(event) => Trans::Quit,
            _ => Trans::None,
        }
    }
}
