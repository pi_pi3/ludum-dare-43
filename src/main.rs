#![allow(clippy::type_complexity)]
extern crate rand;
#[macro_use]
extern crate amethyst;
extern crate amethyst_gltf;
#[macro_use]
extern crate specs_derive;
#[macro_use]
extern crate serde;

use amethyst::{
    prelude::*,
    renderer::{DisplayConfig, DrawSkybox, DrawToonSeparate, Pipeline, RenderBundle, Stage},
    animation::VertexSkinningBundle,
    utils::application_root_dir,
    input::InputBundle,
    core::transform::{Transform, TransformBundle},
    animation::AnimationBundle,
    assets::{PrefabLoaderSystem},
};
use amethyst_gltf::GltfSceneLoaderSystem;

use serde::Deserialize;

use game::{GameDataBuilder, GameState, Gameplay, ScenePrefabData};

mod game;
mod creature;
mod player;
mod goblin;
mod spike;
mod systems;
mod physics;
mod ai;

#[derive(Deserialize, Serialize)]
pub struct ControlsConfig {
    pub name: String,
}

impl ControlsConfig {
    pub fn try_load() -> Option<ControlsConfig> {
        let file = format!("{}/resources/controls.ron", application_root_dir());
        let string = std::fs::read_to_string(&file).ok()?;
        let mut de = ron::de::Deserializer::from_str(&string).ok()?;
        ControlsConfig::deserialize(&mut de).ok()
    }
}

impl Default for ControlsConfig {
    fn default() -> ControlsConfig {
        ControlsConfig {
            name: "keyboard_config.ron".to_string(),
        }
    }
}

/// Program entry point
///
/// Creates the `Application`, `GameData`, initialize bundles.
fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let app_root = application_root_dir();
    let path = format!("{}/resources/display_config.ron", app_root,);

    let config = DisplayConfig::load(&path);
    let pipe = Pipeline::build().with_stage(
        Stage::with_backbuffer()
            .clear_target([0.0, 0.0, 0.0, 1.0], 1.0)
            .with_pass(DrawSkybox::new())
            .with_pass(DrawToonSeparate::new().with_vertex_skinning()),
    );

    let controls = ControlsConfig::try_load().unwrap_or_default();
    let bindings = format!("{}/resources/{}", app_root, controls.name);

    let input_bundle = InputBundle::<String, String>::new().with_bindings_from_file(bindings)?;

    let game_data = GameDataBuilder::new()
        .with_core(PrefabLoaderSystem::<ScenePrefabData>::default(), "scene_loader", &[])
        .with_core(
            GltfSceneLoaderSystem::default(),
            "gltf_loader",
            &["scene_loader"], // This is important so that entity instantiation is performed in a single frame.
        )
        .with_core_bundle(RenderBundle::new(pipe, Some(config)))?
        .with_core_bundle(
            AnimationBundle::<usize, Transform>::new("animation_control", "sampler_interpolation")
                .with_dep(&["gltf_loader"]),
        )?
        .with_core_bundle(TransformBundle::new().with_dep(&["animation_control", "sampler_interpolation"]))?
        .with_core_bundle(VertexSkinningBundle::new().with_dep(&[
            "transform_system",
            "animation_control",
            "sampler_interpolation",
        ]))?
        .with_core_bundle(input_bundle)?
        .with(GameState::Gameplay, systems::ControllerSystem, "controller_system", &[])
        .with(
            GameState::Gameplay,
            systems::PhysicsSystem,
            "physics_system",
            &["controller_system"],
        )
        .with(GameState::Gameplay, systems::AiSystem, "ai_system", &[]);

    let assets_path = format!("{}/assets/", app_root);
    let mut game = Application::new(assets_path, Gameplay::default(), game_data)?;

    game.run();

    Ok(())
}
