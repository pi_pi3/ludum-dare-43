use amethyst::ecs::prelude::*;

use physics::Vec3;
use physics::intersection::*;

#[derive(Clone, Copy, Debug, PartialEq, Component)]
#[storage(VecStorage)]
pub struct Collider {
    pub movable: bool,
    shape: Shape,
}

impl Collider {
    pub fn new(movable: bool, shape: Shape) -> Collider {
        Collider { movable, shape }
    }

    pub fn intersects(&self, other: &Collider) -> Option<Intersection> {
        self.shape.intersects(&other.shape)
    }

    pub fn update_position(&mut self, position: Vec3) {
        self.shape.update_position(position)
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Shape {
    Sphere(Sphere),
}

impl Shape {
    pub fn intersects(&self, other: &Shape) -> Option<Intersection> {
        match (self, other) {
            (&Shape::Sphere(ref a), &Shape::Sphere(ref b)) => check_sphere_sphere(a, b),
        }
    }

    pub fn update_position(&mut self, position: Vec3) {
        match *self {
            Shape::Sphere(ref mut sphere) => sphere.update_position(position),
        }
    }
}

/// Axis-aligned bounding box.
///
/// Defined by a width, height, depth
#[derive(Clone, Copy, Debug, PartialEq, Default)]
pub struct Sphere {
    /// Location in 3d
    pub position: Vec3,

    /// Radius
    pub r: f32,
}

impl Sphere {
    pub fn with_position(r: f32, position: Vec3) -> Sphere {
        Sphere { r, position }
    }

    pub fn update_position(&mut self, position: Vec3) {
        self.position = position;
    }
}
