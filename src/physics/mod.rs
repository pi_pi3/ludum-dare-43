use std::ops::{Add, AddAssign, Sub, SubAssign, Mul, MulAssign};

use amethyst::ecs::prelude::*;

pub use self::shape::*;

pub mod shape;
pub mod intersection;

#[derive(Clone, Copy, Debug, PartialEq, Default, Component)]
#[storage(VecStorage)]
pub struct Rotation(pub f32);

#[derive(Clone, Copy, Debug, PartialEq, Default, Component)]
#[storage(DenseVecStorage)]
pub struct Position(pub Vec3);

#[derive(Clone, Copy, Debug, PartialEq, Default, Component)]
#[storage(VecStorage)]
pub struct Velocity(pub Vec3);

#[derive(Clone, Copy, Debug, PartialEq, Default, Component)]
#[storage(VecStorage)]
pub struct Acceleration(pub Vec3);

#[derive(Clone, Copy, Debug, PartialEq, Default)]
pub struct Vec3 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl Vec3 {
    pub fn new(x: f32, y: f32, z: f32) -> Vec3 {
        Vec3 { x, y, z }
    }
}

impl Mul<f32> for Vec3 {
    type Output = Self;

    fn mul(self, s: f32) -> Vec3 {
        Vec3 {
            x: self.x * s,
            y: self.y * s,
            z: self.z * s,
        }
    }
}

impl MulAssign<f32> for Vec3 {
    fn mul_assign(&mut self, s: f32) {
        self.x *= s;
        self.y *= s;
        self.z *= s;
    }
}

impl Add<Vec3> for Vec3 {
    type Output = Self;

    fn add(self, der: Vec3) -> Vec3 {
        Vec3 {
            x: self.x + der.x,
            y: self.y + der.y,
            z: self.z + der.z,
        }
    }
}

impl AddAssign<Vec3> for Vec3 {
    fn add_assign(&mut self, der: Vec3) {
        self.x += der.x;
        self.y += der.y;
        self.z += der.z;
    }
}

impl Sub<Vec3> for Vec3 {
    type Output = Self;

    fn sub(self, der: Vec3) -> Vec3 {
        Vec3 {
            x: self.x - der.x,
            y: self.y - der.y,
            z: self.z - der.z,
        }
    }
}

impl SubAssign<Vec3> for Vec3 {
    fn sub_assign(&mut self, der: Vec3) {
        self.x -= der.x;
        self.y -= der.y;
        self.z -= der.z;
    }
}

/// Inaccurate, appoximative friction.
#[derive(Clone, Copy, Debug, PartialEq, Component)]
#[storage(VecStorage)]
pub struct Friction(pub f32);

impl Default for Friction {
    fn default() -> Friction {
        Friction(1.0)
    }
}
