use physics::Vec3;
use physics::shape::*;

#[derive(Clone, Copy, Debug)]
pub struct Intersection {
    pub position: Vec3,
    pub offset: Vec3,
}

pub fn check_sphere_sphere(a: &Sphere, b: &Sphere) -> Option<Intersection> {
    let dx = b.position.x - a.position.x;
    let dy = b.position.y - a.position.y;
    let dz = b.position.z - a.position.z;
    let mut d = (dx * dx + dy * dy + dz * dz).sqrt();

    if d == 0.0 {
        d = 0.0001;
    }

    if d > a.r + b.r {
        return None;
    }

    let d_recip = d.recip();
    let dir = Vec3::new(dx * d_recip, dy * d_recip, dz * d_recip);
    let a_in_b = a.position + dir * a.r;
    let b_in_a = b.position + dir * b.r;

    let offset = a_in_b - b_in_a;

    Some(Intersection {
        position: b_in_a,
        offset,
    })
}
