use rand::random;

use amethyst::prelude::*;
use amethyst::ecs::prelude::*;
use amethyst::core::transform::Transform;
use amethyst::ecs::world::LazyBuilder;

use amethyst::assets::Handle;
use amethyst_gltf::GltfSceneAsset;

use crate::physics::{Vec3, Position, Collider, Shape, Sphere};

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Deserialize, Serialize, Component)]
#[storage(NullStorage)]
pub struct SpikesMarker;

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Deserialize, Serialize, Component)]
#[storage(NullStorage)]
pub struct Spike;

impl Spike {
    pub fn with_lazy<'a>(builder: LazyBuilder<'a>, gltf: Handle<GltfSceneAsset>) {
        let x = (random::<f32>() * 2. - 1.) * 64.;
        let z = (random::<f32>() * 2. - 1.) * 64.;
        let pos = Vec3::new(x, 0., z);
        builder
            .with(Spike)
            .with(gltf)
            .with(Transform::default())
            .with(Position(pos))
            .with(Collider::new(true, Shape::Sphere(Sphere::with_position(0.45, pos))))
            .build();
    }
}
