use amethyst::prelude::*;
use amethyst::ecs::prelude::*;
use amethyst::ecs::world::LazyBuilder;
use amethyst::core::transform::Transform;
use amethyst::assets::Handle;
use amethyst_gltf::GltfSceneAsset;

use game::Consts;
use creature::Creature;
use physics::{Vec3, Rotation, Position, Velocity, Acceleration, Friction, Collider, Shape, Sphere};

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Deserialize, Serialize, Component)]
#[storage(NullStorage)]
pub struct PlayerMarker;

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Deserialize, Serialize, Component)]
#[storage(NullStorage)]
pub struct Zmobie;

#[derive(Debug, Default, Clone, Copy, PartialEq, Component)]
#[storage(NullStorage)]
pub struct Player;

impl Player {
    pub fn with_lazy<'a>(builder: LazyBuilder<'a>, gltf: Handle<GltfSceneAsset>, consts: &Consts) {
        let e = builder.entity;
        builder
            .with(Player)
            .with(Creature {
                entity: e,
                health: consts.player_health,
                iframes: 0.,
                iframes_const: consts.player_iframes,
                animation: None,
                run_animation: consts.player_run_animation,
            })
            .with(gltf)
            .with(Transform::default())
            .with(Rotation::default())
            .with(Position::default())
            .with(Velocity::default())
            .with(Acceleration::default())
            .with(Friction(consts.player_friction))
            .with(Collider::new(
                true,
                Shape::Sphere(Sphere::with_position(0.25, Vec3::default())),
            ))
            .build();
    }

    pub fn zmobiefy<'a>(e: Entity, zmobies: &mut WriteStorage<'a, Zmobie>) {
        zmobies.insert(e, Zmobie).unwrap();
    }
}
